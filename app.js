particlesJS('particles-js', {
    "particles": {
      "number": {
        "value": 200, // زيادة عدد الألعاب النارية
        "density": {
          "enable": true,
          "value_area": 800
        }
      },
      "color": {
        "value": "#00F" // لون النار
      },
      "shape": {
        "type": "circle",
        "stroke": {
          "width": 0,
          "color": "#fff"
        }
      },
      "opacity": {
        "value": 0.5,
        "random": true,
        "anim": {
          "enable": false,
          "speed": 1,
          "opacity_min": 0.1,
          "sync": false
        }
      },
      "size": {
        "value": 5,
        "random": true
      },
      "line_linked": {
        "enable": false
      },
      "move": {
        "enable": true,
        "speed": 3,
        "direction": "bottom",
        "random": false,
        "straight": false,
        "out_mode": "out",
        "bounce": false
      }
    },
    "interactivity": {
      "detect_on": "canvas",
      "events": {
        "onhover": {
          "enable": false,
          "mode": "repulse"
        },
        "onclick": {
          "enable": false,
          "mode": "push"
        },
        "resize": true
      }
    },
    "retina_detect": true
  });
  
  // يمكنك استخدام setTimeout لعرض عبارة "Happy Birthday" بعد مدة زمنية مناسبة
  setTimeout(function() {
    document.getElementById('birthday-message').style.opacity = '1';
  }, 10000); // هذا يعرض الرسالة بعد 10 ثواني
  